import os
import json
import time
import calendar
import datetime
from tornado import ioloop,web
import urllib
import redis,ast
import hashlib
from memsql.common import database
import MySQLdb
import ast
import operator
from pymongo import MongoClient
from elasticsearch import Elasticsearch

gnip_data = MongoClient("mongodb://104.155.210.134/GnipDataFinal",27017)["GnipDataFinal"]["tweets"]

# Specify connection information for a MemSQL node
HOST     = "memsql.paralleldots.com"
USER     = "ankit"
PASSWORD = "mongodude123"

# Specify which database and table to work with.
# Note: this database will be dropped at the end of this script
DATABASE = "socialmedia"

def get_connection(db=DATABASE):
    return database.connect(host=HOST,user=USER, password=PASSWORD, database=db)

class TwitterHashtagHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        
        tweets     = []  
        pageno     = int(self.get_argument('page'))
        hashtag    = self.get_argument('hashtag')
        alertid    = self.get_argument('alertid')

        with get_connection(db=DATABASE) as conn:
            alert_data   = conn.query('SELECT created_at FROM myalerts where id=%s',alertid)
            tagname_data = conn.query('SELECT tagname FROM twittertrack where alertid=%s',alertid)
        
        data = gnip_data.find({'tweet.timestamp':{'$gte':alert_data[0].get('created_at')},'tagname':tagname_data[0].get('tagname'),'tweet.entity.hashtags':{'$elemMatch':{'text':hashtag}}},{'tweet.text':1,'tweet.tweet_link':1,'tweet.user':1,'tweet.timestamp':1,'tweet.retweet_count':1,'tweet.favorite_count':1})
        count = data.count()
        data.skip((pageno-1)*20).limit(20)
        
        for obj in data:
            tweets.append({'text':obj.get('tweet').get('text'),'timestamp':str(obj.get('tweet').get('timestamp')),'retweet_count':obj.get('tweet').get('retweet_count'),'favorite_count':obj.get('tweet').get('favorite_count'),'url':obj.get('tweet').get('tweet_link').split('//')[1],'screen_name':obj.get('tweet').get('user').get('screen_name'),'profile_image_url':obj.get('tweet').get('user').get('image'),'username':obj.get('tweet').get('user').get('name')})
        
        conn.close()
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        if (pageno*20) >= count:
            self.finish(json.dumps({'Status':1,'counter':count,'tweets':tweets,'hashtag':hashtag}))
            return
        else:
            pageno = pageno+1
            self.finish(json.dumps({'Status':1,'counter':count,'tweets':tweets,'hashtag':hashtag,'page':'http://utilities.paralleldots.com/twitter/hashtag?alertid='+alertid+'&hashtag='+hashtag+'&page='+str(pageno)}))
            return

settings = {
    "template_path": os.path.join(os.path.dirname(__file__), "templates"),
    "static_path": os.path.join(os.path.dirname(__file__), "static"),
    "debug" : False
}

application = web.Application([
    (r'/twitter/hashtag', TwitterHashtagHandler),
],**settings)

if __name__ == "__main__":
    print "Here we go"
    application.listen(9090)
    ioloop.IOLoop.instance().start()